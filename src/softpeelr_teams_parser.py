import re
from html.parser import HTMLParser
from datetime import datetime
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('tournament-print.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)


class SoftPeelRTeamsHTMLParser(HTMLParser):
    teams = {}

    parsing_teams = False
    parsing_team = False
    data_is_team_name = False
    parsing_team_members = False
    data_is_team_member = False
    team_name = ""
    idx = 0

    def clear(self):
        self.reset()
        self.teams = {}

        self.parsing_teams = False
        self.parsing_team = False
        self.data_is_team_name = False
        self.parsing_team_members = False
        self.data_is_team_member = False
        self.team_name = ""
        self.idx = 0

    def handle_starttag(self, tag, attrs):
        if tag == 'div':
            for attr in attrs:
                if attr[0] == 'class' and 'registered-teams-row' in attr[1]:
                    self.parsing_teams = True
            if self.parsing_teams:
                self.idx += 1
        if self.parsing_teams and tag == 'h3':
            self.parsing_team = True
        if self.parsing_team and tag == 'a':
            self.data_is_team_name = True
        if self.parsing_teams and tag == 'ul':
            self.parsing_team_members = True
        if self.parsing_team_members and tag == 'div':
            for attr in attrs:
                if attr[0] == 'class' and attr[1] == 'col-xs-8':
                    self.data_is_team_member = True

    def handle_endtag(self, tag):
        if tag == 'div' and self.parsing_teams:
            self.idx -= 1
            if self.idx == 0:
                self.parsing_teams = False
        if tag == 'ul':
            self.parsing_team_members = False

    def handle_data(self, data):
        if self.data_is_team_name:
            self.team_name = data
            self.data_is_team_name = False
            self.teams[self.team_name] = []
        if self.data_is_team_member:
            self.teams[self.team_name].append(
                re.sub("\s\s+", " ", data.strip()))
            self.data_is_team_member = False

    def get_teams(self):
        return self.teams
