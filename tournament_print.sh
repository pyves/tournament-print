#!/bin/bash
# PYTHON_ARGCOMPLETE_OK

CURRENT_VERSION="0.1"

WORKDIR="/home/pierre-yves/eclipse-perso-workspace/tournament-print"

VENV="$WORKDIR/venv"
VERSION="$WORKDIR/venv_version"
PIP="$VENV/bin/pip"
PYTHON="$VENV/bin/python"
SCRIPT_PATH=`realpath "$0"`
RUN_PATH=`dirname $SCRIPT_PATH`
echo "Use virtualenv : $VENV"

function rebuild_venv {
    rm -Rf $VENV
    python3 -m virtualenv -p python3 "$VENV"
    $PIP install -r $RUN_PATH/requirements.txt
    echo "$CURRENT_VERSION" > $VERSION
}

if [ ! -d "$VENV" ]; then
    # VENV is not a directory
    rebuild_venv
elif [ ! -f "$VERSION" ]; then
    # VERSION is not a file
    rebuild_venv
elif [ "$(cat $VERSION)" != "$CURRENT_VERSION" ]; then
    # VERSION is not the expected one, this is to force venv rebuild
    # when script version change.
    rebuild_venv
fi

$PYTHON $RUN_PATH/src/tournament_print.py $@
