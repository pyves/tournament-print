from html.parser import HTMLParser
from datetime import datetime
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('tournament-print.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)


class SoftPeelRScheduleHTMLParser(HTMLParser):
    tournament_dates = []
    page_matches = []

    parsing_dates = False

    parsing_matches = False
    parsing_sheets = False
    data_is_date = False
    data_is_time = False
    match_date = ""
    match_time = ""
    idx = 0
    data_is_match = False
    sheets = []

    def clear(self):
        self.reset()
        self.tournament_dates = []
        self.page_matches = []

        self.parsing_dates = False

        self.parsing_matches = False
        self.parsing_sheets = False
        self.data_is_date = False
        self.data_is_time = False
        self.match_date = ""
        self.match_time = ""
        self.idx = 0
        self.data_is_match = False
        self.sheets = []

    def handle_starttag(self, tag, attrs):
        if tag == 'select':
            for attr in attrs:
                if attr[0] == 'name' and attr[1] == 'date':
                    self.parsing_dates = True
        if self.parsing_dates and tag == 'option':
            for attr in attrs:
                if attr[0] == 'value':
                    self.tournament_dates.append(
                        datetime.strptime(attr[1], '%Y-%m-%d'))
        if tag == 'h4':
            self.data_is_date = True
        if tag == 'div':
            for attr in attrs:
                if attr[0] == 'class' and attr[1] == 'schedule-table':
                    self.parsing_matches = True
        if self.parsing_matches:
            if tag == 'tr':
                self.idx = 0
            if tag == 'th':
                if self.idx != 0:
                    self.parsing_sheets = True
                self.idx += 1
            if tag == 'td':
                if self.idx == 0:
                    self.data_is_time = True
                self.idx += 1
            if tag == 'a' and self.idx > 0:
                for attr in attrs:
                    if attr[0] == 'href' and 'matches' in attr[1]:
                        self.data_is_match = True

    def handle_endtag(self, tag):
        if tag == 'select' and self.parsing_dates:
            self.parsing_dates = False
        if self.data_is_date and tag == 'h4':
            self.data_is_date = False
        if self.parsing_sheets and tag == 'th':
            self.parsing_sheets = False
        if self.data_is_time and tag == 'td':
            self.data_is_time = False
        if self.data_is_match and tag == 'a':
            self.data_is_match = False

    def handle_data(self, data):
        if self.parsing_sheets and self.idx > 0:
            self.sheets.append(data)
        if self.data_is_date:
            self.match_date = data
        if self.data_is_time:
            self.match_time = data
        if self.data_is_match:
            teams = data.split(" v ")
            logger.debug(f"Sheets : {self.sheets} {self.idx}")
            match = {
                'date': f"{self.match_date}T{self.match_time}",
                'rink': self.sheets[self.idx-2],
                'team1': teams[0],
                'team2': teams[1]}
            logger.debug(f"Add match {match}")
            self.page_matches.append(match)

    def get_tournament_dates(self):
        return self.tournament_dates

    def get_matches(self):
        return self.page_matches
