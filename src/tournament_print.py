# PYTHON_ARGCOMPLETE_OK - Needed to activate argcomplete on this script !
import argparse
import argcomplete
import yaml
from datetime import datetime, timedelta
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.lib import colors
from reportlab.platypus.tables import Table
from softpeelr_schedule_parser import (
    SoftPeelRScheduleHTMLParser  # @UnresolvedImport
)
from softpeelr_teams_parser import (
    SoftPeelRTeamsHTMLParser  # @UnresolvedImport
)
from urllib.request import Request, urlopen
from urllib.error import URLError

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('tournament-print.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)


class TournamentPrint:

    data = None
    with_breaks = False
    margins = None

    def __init__(self, args):
        with open(args.source, 'r') as stream:
            self.data = yaml.safe_load(stream)
        logger.debug("Input data : %s" % (self.data,))
        self.margins = {
            'left': 10*mm,
            'right': 10*mm,
            'top': 20*mm,
            'bottom': 15*mm}
        if args.margins:
            with open(args.margins, 'r') as stream:
                margins = yaml.safe_load(stream)
                if 'left' in margins:
                    self.margins['left'] = int(margins['left'])*mm
                if 'right' in margins:
                    self.margins['right'] = int(margins['right'])*mm
                if 'top' in margins:
                    self.margins['top'] = int(margins['top'])*mm
                if 'bottom' in margins:
                    self.margins['bottom'] = int(margins['bottom'])*mm
        logger.debug("Page settings : %s" % (self.margins))

    def get_matches(self, date_from, date_to):
        parser = SoftPeelRScheduleHTMLParser()
        print(f"data: {self.data}")
        for group_name in self.data['groups'].keys():
            group = self.data['groups'][group_name]
            req = Request(f"{group['url']}/schedule")
            try:
                page = urlopen(req)
            except URLError as e:
                if hasattr(e, 'reason'):
                    logger.exception(
                        'We failed to reach a server. Reason: %s', e.reason, e)
                elif hasattr(e, 'code'):
                    logger.exception(
                        'The server couldn\'t fulfill the request. '
                        'Error code:', e.code, e)
                else:
                    logger.exception('Exception occurred.', e)
            else:
                parser.feed(page.read().decode('utf-8'))
                dates = parser.get_tournament_dates()
                parser.clear()
            for match_date in dates:
                if (
                        match_date.date() >= date_from.date() and
                        match_date.date() <= date_to.date()):
                    req = Request(
                        f"{group['url']}/schedule?date={match_date:%Y-%m-%d}")
                    try:
                        page = urlopen(req)
                    except URLError as e:
                        if hasattr(e, 'reason'):
                            logger.exception(
                                'We failed to reach a server. '
                                'Reason: %s', e.reason, e)
                        elif hasattr(e, 'code'):
                            logger.exception(
                                'The server couldn\'t fulfill the request. '
                                'Error code:', e.code, e)
                        else:
                            logger.exception('Exception occurred.', e)
                    else:
                        parser.feed(page.read().decode('utf-8'))
                        matches = parser.get_matches()
                        logger.debug(f"Matches : {matches}")
                        parser.clear()
                        for match in matches:
                            logger.debug(f"Match is {match}")
                            if 'matches' not in self.data:
                                self.data['matches'] = {}
                            if match['date'] not in self.data['matches']:
                                self.data['matches'][match['date']] = []
                            match['group'] = group_name
                            self.data['matches'][match['date']].append(match)

    def get_teams(self):
        parser = SoftPeelRTeamsHTMLParser()
        for group_name in self.data['groups'].keys():
            group = self.data['groups'][group_name]
            req = Request(f"{group['url']}/team_instances")
            try:
                page = urlopen(req)
            except URLError as e:
                if hasattr(e, 'reason'):
                    logger.exception(
                        'We failed to reach a server. Reason: %s', e.reason, e)
                elif hasattr(e, 'code'):
                    logger.exception(
                        'The server couldn\'t fulfill the request. '
                        'Error code:', e.code, e)
                else:
                    logger.exception('Exception occurred.', e)
            else:
                parser.feed(page.read().decode('utf-8'))
                self.data['groups'][group_name]['teams'] = parser.get_teams()
                parser.clear()

    def get_match_height(self, match):
        if match['team1'] in self.data['groups'][match['group']]['teams']:
            members1 = self.data[
                'groups'][match['group']]['teams'][match['team1']]
        else:
            members1 = []
        if match['team2'] in self.data['groups'][match['group']]['teams']:
            members2 = self.data[
                'groups'][match['group']]['teams'][match['team2']]
        else:
            members2 = []
        member_nb = max(len(members1), len(members2))
        return sum(
            [10*mm, 6*mm, 10*mm, 10*mm, 10*mm]) + (member_nb + 2) * 6 * mm

    def print_match(self, match, moment, c, x, y):
        width, height = A4
        avail_width = width - self.margins['left'] \
            - self.margins['right'] - 60 * mm - 10 * mm
        ends = self.data['groups'][match['group']]['nb_ends'] + 1
        if self.data['groups'][match['group']]['with_extra']:
            ends += 1
        handicap = 0
        if self.data['groups'][match['group']]['with_handicap']:
            ends += 1
            handicap = 1
        cw = avail_width / (ends + 2)
        cell_widths = [60*mm]
        if match['team1'] in self.data['groups'][match['group']]['teams']:
            members1 = self.data[
                'groups'][match['group']]['teams'][match['team1']]
        else:
            members1 = []
        if match['team2'] in self.data['groups'][match['group']]['teams']:
            members2 = self.data[
                'groups'][match['group']]['teams'][match['team2']]
        else:
            members2 = []
        member_nb = max(len(members1), len(members2))
        data = [[self.data['groups'][match['group']]['name']],
                [""], [match['team1']], [match['team2']],
                ["Signatures"]]
        if member_nb:
            for i in range(member_nb + 1):
                data.append([""])
        for e in range(ends + 3):
            cell_widths.append(cw)
            data[0].append("")
            data[1].append(e - handicap)
            data[2].append("")
            data[3].append("")
            data[4].append("")
            if member_nb:
                for i in range(member_nb+1):
                    data[5+i].append("")
        data[0][1] = f"{moment:%d.%m.%Y %Hh%M}"
        data[0][-5] = match['rink']
        data[1][1] = "T"
        if self.data['groups'][match['group']]['with_handicap']:
            data[1][2] = "H"
        if self.data['groups'][match['group']]['with_extra']:
            data[1][-4] = "E"
        data[1][-3] = "Total"
        data[4][1] = "Gagnant"
        data[4][-7] = "Perdant"
        logger.debug(f"Data : {data}")
        cell_heights = [10*mm, 6*mm, 10*mm, 10*mm, 10*mm]
        styles = [('GRID', (0, 1), (-1, 3), 0.3, colors.gray),
                  ('BOX', (0, 0), (-1, 4), 1, colors.black),
                  ('SPAN', (1, 0), (-6, 0)),
                  ('SPAN', (-5, 0), (-1, 0)),
                  ('SPAN', (-3, 1), (-1, 1)),
                  ('SPAN', (1, 4), (-8, 4)),
                  ('SPAN', (-7, 4), (-1, 4)),
                  ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                  ('ALIGN', (0, 1), (0, 1), 'RIGHT'),
                  ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
                  ('FONTNAME', (0, 0), (1, 0), 'Helvetica-Bold'),
                  ('FONTNAME', (1, 1), (-1, 1), 'Helvetica-Bold'),
                  ('ALIGN', (1, 1), (-1, 1), 'CENTER'),
                  ]
        if self.data['groups'][match['group']]['result_type'] == 'NB_STONE':
            styles.extend([
                ('SPAN', (-3, 2), (-1, 2)),
                ('SPAN', (-3, 3), (-1, 3))])
        member_height = 0
        if member_nb:
            for i in range(member_nb+1):
                cell_heights.append(6*mm)
                member_height += 6*mm
            data[5][0] = f"{match['team1']}"
            data[5][4] = f"{match['team2']}"
            styles.append(('FONTNAME', (0, 5), (-1, 5), 'Helvetica-Bold'))
            for i in range(member_nb):
                if len(members1) > i:
                    # print(f"member1 {members1[i]}")
                    data[6+i][0] = f"  {members1[i]}"
                else:
                    pass
                    # print(f"Nothing to print for {i}")
                if len(members2) > i:
                    # print(f"member2 {members2[i]}")
                    data[6+i][4] = f"  {members2[i]}"
                else:
                    pass
                    # print(f"Nothing to print for {i}")
        t = Table(
            data, style=styles, colWidths=cell_widths, rowHeights=cell_heights)
        t.wrapOn(c, width, height/4)
        t.drawOn(c, x + self.margins['left'], y)
        return sum(cell_heights) + 6*mm

    def print_new_page(self, c):
        c.drawString(
            self.margins['left'],  self.margins['bottom'],
            "T : dernière pierre au 1er end - H : points de "
            "bonus pour remplaçant - E : extra-end")
        c.showPage()

    def print_sheets(
            self, date_from, date_to, new_page, filename):
        if not date_from:
            date_from = datetime.now()
        if not date_to:
            date_to = date_from + timedelta(days=6)
        print(
            f"Printing sheets from {date_from} to "
            f"{date_to}")
        self.get_matches(date_from, date_to)
        self.get_teams()
        logger.debug("Data : %s" % (self.data,))
        if 'matches' not in self.data:
            logger.warning("No match defined")
            return
        c = canvas.Canvas(filename, pagesize=A4)
        width, height = A4
#        c.translate(0, height-20*mm)
        previous = self.margins['top']
        dates = list(self.data['matches'].keys())
        dates.sort()
        for d in dates:
            match_date = datetime.strptime(d, '%Y-%m-%dT%H:%M')
            if match_date < date_from or match_date > date_to:
                logger.warning(
                    f"Date not in range {match_date} {date_from} {date_to}")
                continue
            matches = sorted(self.data['matches'][d], key=lambda k: k['rink'])
            for m in matches:
                match_height = self.get_match_height(m)
                if (height - match_height - previous <
                        13*mm+self.margins['bottom']):
                    self.print_new_page(c)
                    previous = self.margins['top']
                previous += self.print_match(
                    m, match_date, c, 0,
                    height - match_height + 6*mm - previous)
            if new_page and previous != self.margins['top']:
                self.print_new_page(c)
                previous = self.margins['top']
        # c.drawString(10*mm, 10*mm, "Matches")
        if previous != self.margins['top']:
            self.print_new_page(c)
        c.save()


def parse_date(date_str):
    if not date_str:
        return None
    if 'T' in date_str:
        return datetime.strptime(date_str, '%Y-%m-%dT%H:%M')
    else:
        return datetime.strptime(date_str, '%Y-%m-%d')


if __name__ == "__main__":
    # ./tournament_print.sh -p -o ~/Bureau/matches.pdf -s neuch.yml \
    # 2020-10-26 2020-11-01
    parser = argparse.ArgumentParser(
        description="""
TournamentPrint is a script that allows to print different aspects of a
curling tournament.""")
    parser.add_argument(
        '-s', '--source', default='source.yml',
        help="the source configuration for tournament")
    parser.add_argument(
        '-p', '--new-page', dest='new_page',
        help="New page for each date", action="store_true", default=False)
    parser.add_argument(
        '-o', '--out-filename', dest='filename',
        help="Destination file name", default='matches.pdf')
    parser.add_argument(
        '-m', '--margins', help="Override printer settings (margins)")
    parser.add_argument('date_from', nargs='?')
    parser.add_argument('date_to', nargs='?')
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    printer = TournamentPrint(parser.parse_args())
    printer.print_sheets(
        parse_date(args.date_from), parse_date(args.date_to), args.new_page,
        args.filename)
